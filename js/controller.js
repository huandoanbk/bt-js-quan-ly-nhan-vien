function getID(ID) {
  return document.getElementById(ID);
}

//Reset modal on hide
$("#myModal").on("hidden.bs.modal", function (e) {
  $(".modal-body input").val(""); //reset all input
});

//btnThem(): Đổi Header title, ẩn nút cập nhật
function btnThem() {
  getID("header-title").innerText = "Thêm Nhân viên";
  getID("btnCapNhat").classList.add("d-none");
  getID("btnThemNV").classList.remove("d-none");
}
//Lấy thông tin từ form
function getFormdata() {
  let username = getID("tknv").value;
  let hoten = getID("name").value;
  let email = getID("email").value;
  let pass = getID("password").value;
  let ngaylam = getID("datepicker").value;
  let luongCB = getID("luongCB").value;
  let chucvu = getID("chucvu").value;
  let giolam = getID("gioLam").value;

  return {
    username: username,
    hoten: hoten,
    email: email,
    pass: pass,
    ngaylam: ngaylam,
    luongCB: luongCB,
    chucvu: chucvu,
    giolam: giolam,
    tongluong: tinhLuong(luongCB, chucvu),
    loainv: xepLoai(giolam),
  };
}

//Render DSNV ra table
function renderDSNV(dsnv) {
  let contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    let item = dsnv[i];
    let contentTR = `
        <tr>
        <td>${item.username}</td>
        <td>${item.hoten}</td>
        <td>${item.email}</td>
        <td>${item.ngaylam}</td>
        <td>${item.chucvu}</td>
        <td>${item.tongluong}</td>
        <td>${item.loainv}</td>
        <td class="d-flex">
             <button
             data-toggle="modal"
             data-target="#myModal"
             onclick=updateModal(${i})
             class="btn btn-info mr-1">Cập nhật</button>
            <button
             onclick=xoaNV('${item.username}')
             class="btn btn-danger">Xoá</button>
        </td>
        </tr>
        `;
    contentHTML += contentTR;
  }
  getID("tableDanhSach").innerHTML = contentHTML;
}
