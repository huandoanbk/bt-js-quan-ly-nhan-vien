var dsnv = []; //array Danh sách nhân viên

//khi user load trang => lấy dữ liệu từ localStorage
const DSNV_LOCAL = "DSNV_LOCAL";
var jsonData = localStorage.getItem(DSNV_LOCAL);
if (jsonData != null) {
  dsnv = JSON.parse(jsonData);
  renderDSNV(dsnv);
}
console.log(dsnv);

// Thêm nhân viên
function themNhanvien() {
  let nhanvien = getFormdata();
  dsnv.push(nhanvien);
  renderDSNV(dsnv); //render danh sách nhân viên ra table
  let dsvnJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV_LOCAL", dsvnJson);
  $("#myModal").modal("hide");
}

// Tính lương dựa vào lương căn bản và chức vụ
function tinhLuong(luongcb, chucvu) {
  switch (chucvu) {
    case "Giám đốc":
      return luongcb * 3;
      break;
    case "Trưởng phòng":
      return luongcb * 2;
      break;
    case "Nhân viên":
      return luongcb;
      break;
    default:
      return null;
  }
}

// Xếp loại nhân viên
function xepLoai(giolam) {
  if (giolam >= 192) {
    return "Xuất sắc";
  } else if (giolam >= 176) {
    return "Giỏi";
  } else if (giolam >= 160) {
    return "Khá";
  } else {
    return "Trung bình";
  }
}

//Xoá nhân viên
function xoaNV(id) {
  if (confirm("Bạn có muốn xoá?")) {
    let viTri = dsnv.findIndex(function (item) {
      return item.username == id;
    });

    dsnv.splice(viTri, 1);
    let dsvnJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dsvnJson);
    renderDSNV(dsnv);
  }
}

//Cập nhật nhân viên
var updateID;
function updateModal(id) {
  getID("header-title").innerText = "Cập nhật Nhân viên";
  getID("btnCapNhat").classList.remove("d-none");
  getID("btnThemNV").classList.add("d-none");

  // let viTri = dsnv.findIndex(function (item) {
  //   return item.username == id;
  // });
  let sv = dsnv[id];
  getID("tknv").value = sv.username;
  getID("name").value = sv.hoten;
  getID("email").value = sv.email;
  getID("password").value = sv.pass;
  getID("datepicker").value = sv.ngaylam;
  getID("luongCB").value = sv.luongCB;
  getID("chucvu").value = sv.chucvu;
  getID("gioLam").value = sv.giolam;
  updateID = id;
}

function updateNV() {
  let nv = getFormdata();
  //  let viTri = dsnv.findIndex(function (item) {
  //    return item.username == nv.username;
  //  });
  dsnv[updateID] = nv;
  let dsvnJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV_LOCAL", dsvnJson);
  renderDSNV(dsnv);
  $("#myModal").modal("hide");
}
